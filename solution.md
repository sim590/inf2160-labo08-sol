# INF2160 - Labo07 - Solutions

## 1 - Introduction

Petite observation faite en classe: on peut se créer un foncteur permettant de
reconnaître les bons légumes.

```prolog
legume_sucre(X) :- aliment(X, legume), gout(X, sucre).
```

Dans `swipl`, vous pouvez alors listez les bons légumes:

```
?- legume_sucre(X)
|    .
X = navet
```

## 2 - Listes

### 2.1 - `concat`

> Définissez un prédicat `concat(L1, L2, L)`, qui indique si `L` est la liste
> obtenue en concaténant `L1` et `L2`.
>
> On s'attend donc au comportement suivant:
>
> ```prolog
> ?- concat([], [], L).
> L = [].
>
> ?- concat([1,2,3], [4,5], L).
> L = [1, 2, 3, 4, 5].
>
> ?- concat([1,2],[2,1], L).
> L = [1, 2, 2, 1].
>
> ?- concat([1,2,3], L, [1,2,3,4,5]).
> L = [4, 5].
>
> ?- concat([1,2,3], L, [4,5]).
> false.
> ```

```prolog
concat([], L2, L2).
concat([X|Xs], L2, [X|Ys]) :-
    concat(Xs, L2, Ys).
```

### 2.2 - `renverse`

> Définissez un prédicat `renverse(L1, L2)`, qui indique si `L1` est la liste
> obtenue de `L2` en renversant l'ordre dans lequel les éléments sont écrits.
>
> *Indice*: N'hésitez pas à utiliser le prédicat `concat` défini plus haut.
>
> ```prolog
> ?- renverse([], []).
> true.
>
> ?- renverse([1,2],[2,1]).
> true.
>
> ?- renverse([alice], [alice]).
> true.
>
> ?- renverse([alpha, beta], [alpha, beta]).
> false.
> ```

```prolog
renverse([], []).
renverse([X|Xs], L) :-
    renverse(Xs, Ys),
    concat(Ys, [X], L).
```

### 2.3 - `palindrome`

> Définissez un prédicat `palindrome(L)`, qui indique si `L` est une liste
> palindromique, c'est-à-dire que le premier élément est égal au dernier, le
> deuxième à l'avant-dernier, etc.
>
> *Indice*: N'hésitez pas à utiliser le prédicat `concat` défini plus haut.
>
> On s'attend donc au comportement suivant:
> ```prolog
> ?- palindrome([]).
> true.
>
> ?- palindrome([alice]).
> true.
>
> ?- palindrome([1,2,3]).
> false.
>
> ?- palindrome([1,2,1]).
> true.
>
> ?- palindrome([alpha, beta, beta, alpha]).
> true.
> ```

```prolog
palindrome([]).
palindrome([_]).
palindrome(L) :-
    concat([X|Xs], [X], L),
    palindrome(Xs).
```

ou bien comme proposé en classe:

```prolog
palindrome2(L) :- renverse(L, L).
```

## 3 - Modélisation

> En géométrie de base, il existe différents types d'objets en 2D. Par exemple,
>
> - Un *cercle*;
> - Un *polygone*, qui a un nombre arbitraire de côtés (au moins 3);
> - Un *triangle*, qui est un polygone de 3 côtés;
> - Un *quadrilatère*, qui est un polygone de 4 côtés;
>
> Parmi les quadrilatères, on peut raffiner le type également:
>
> - Un *trapèze*, qui a au moins 1 paire de côtés parallèles;
> - Un *parallélogramme*, qui a 2 paires de côtés parallèles;
> - Un *rectangle*, qui a 4 angles droits;
> - Un *losange*, qui a 4 côtés de même longueur et 2 paires de côtés parallèles;
> - Un *carré*, qui a 4 côtés de même longueur et 4 angles droits;
>
> Supposons que les atomes qui nous intéressent sont
> ```prolog
> cercle
> polygone
> triangle
> quadrilatere
> trapeze
> parallelogramme
> rectangle
> losange
> carre
> ```
> En utilisant des règles bien choisies, proposez un modèle pour ces différents
> objets qui supporte les requêtes suivantes:
> ```prolog
> est_cercle(X)
> est_polygone(X)
> est_triangle(X)
> est_quadrilatere(X)
> est_trapeze(X)
> est_parallelogramme(X)
> est_rectangle(X)
> est_losange(X)
> ```
> N'oubliez pas qu'un carré est un cas particulier de rectangle et de losange.
> Par contre, un carré n'est pas un cercle.
>
> Évidemment, il est possible de définir ces fonctions en faisant tous les cas
> possibles, mais il existe une solution plus intéressante qui permet de faire de
> l'inférence. Par exemple, on sait que si `X` est un rectangle, alors c'est
> forcément un parallélogramme, un trapèze et un quadrilatère.

```prolog
est_cercle(cercle).

est_polygone(polygone).
est_polygone(X) :- est_triangle(X).
est_polygone(X) :- est_quadrilatere(X).

est_triangle(triangle).

est_quadrilatere(quadrilatere).
est_quadrilatere(X) :- est_trapeze(X).

est_trapeze(trapeze).
est_trapeze(X) :- est_parallelogramme(X).

est_parallelogramme(parallelogramme).
est_parallelogramme(X) :- est_rectangle(X).
est_parallelogramme(X) :- est_losange(X).

est_rectangle(rectangle).
est_rectangle(carre).

est_rectangle(X) :-
    est_rectangle(rectangle);
    est_rectangle(carre).

est_losange(losange).
est_losange(carre).
```

<!-- vim: set ts=4 sw=4 tw=80 et :-->

